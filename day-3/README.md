# Day 3

## Problem Breakdown

* The input is an arbitrarily large square
* Locate all numbers (1-4 digits) next to a non "." symbol, including diagonals
* Sum these numbers
* The total sum is the answer

NOTE: This solution is hyper optimized only for the real input. That being said, it still parses the real input and everything, it just makes certain assumptions and won't work on squares of different sizes


## Solve

This problem is way too large to solve before day 4 comes out, so I will come back for this one after AoC ends.

`ebp` - Total sum
`rsi` - End of input pointer
`rdi` - Current input pointer
`r8` - 0x2E2E2E2E2E2E2E2E


## Notes

# Advent of Code 2023 - Raw Machine Code

This repository will hold my attempts at solving [Advent of Code 2023](https://adventofcode.com/2023) problems using raw machine code.

I will handwrite x86_64 ELF executables meant for Linux systems using only a hex editor.

I will probably not post write ups of any of the solutions in real time, but I will likely go back after this is done and add some. I will not do part 2s of most problems, unless they are very easy. It is very possible though that I will come back later for the part 2s.

During the event, I will add a set of notes that I used to actually create the exectable, and some performance benchmarks and other stats on my solutions.

To execute these binaries, simply run `$ ./bin < input-file-name-here`. Each day I will save a copy of the example input described in the problem in a file named `test-input`, and I will save the real input used to solve the problem in a file named `real-input`.



## Benchmarks

| Day                                                                                                     | Size | Instructions | Cycles   |
|---------------------------------------------------------------------------------------------------------|------|--------------|----------|
| [1](https://gitlab.com/AlphaOmegaProgrammer/aoc-2023-raw-machine-code/-/tree/master/day-1#benchmarks)   | 247  | 223,075      | 190,000  |
| [2](https://gitlab.com/AlphaOmegaProgrammer/aoc-2023-raw-machine-code/-/tree/master/day-2#benchmarks)   | 383  | 26,770       | 30,000   |
| 3                                                                                                       | -    | -            | -        |
| [4](https://gitlab.com/AlphaOmegaProgrammer/aoc-2023-raw-machine-code/-/tree/master/day-4#benchmarks)   | 365  | 167,388      | 108,252  |



## Notes

### Benchmarks

**TL;DR** - Benchmarks are never 100% accurate, and have a lot of variance. Also, becuase the executable reads from stdin, I can't use `perf stat -r`.

Given the timescale that I'm benchmarking on, there is a huge potential variance in my bernchmarks. This is why it's a good idea to take an average over many many executions.

The `perf` command has a `-r` option to repeat the execution multiple times, however, since the executable reads from `STDIN`, this doesn't work because `perf` doesn't hand the input every time it executes the file, only the first time. There is a work around of just using `sh -c "./bin < input-file-name-here"`, however that has the massive overhead of starting a whole new shell every iteration, which significantly skews my results.

What I've done instead is just run the perf command a bunch of times manually, and tried to pick a run that looks close to what an average execution would look like, and tried to estimate the average number of cycles per run. If you know how I can get these numbers without estimating, please let me know.

### Program Stack

**TL;DR** - If the executable segfaults, try running with `env -i ./bin < input-file-name-here`.

These executables try to figure out the starting stack address that is pre-allocated by the kernel, to avoid allocating additional memory. On Linux, this stack address is randomlized, and is around 0x20000 bytes (128KB) in total length. This space is used to store environment variables and any arguments passed to the executable.

Since this address is randomized, I have to kind of guess to where it is unless I want to spend a lot of time calculating its exact starting address. I guess the stack address by zeroing out the lowest 16 bits of the starting stack address, which aligns it to 64KB, then I subtract `0x10000` (another 64KB). This works perfectly fine on my system (TM), howeevr if the binaries segfault on your system, I would assume that either your environment is huge (greater than 64KB) or your stack space is not aligned to 64KB memory addresses.

If your environment is huge, you can clear your environmeny variables for a single command by using `env -i`. If your stack space isn't aligned to 64KB, then I guess these executables just won't work on your system unfortunately.

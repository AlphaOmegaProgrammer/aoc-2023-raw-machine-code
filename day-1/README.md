# Day 1

## Problem Breakdown

* The input is a series of strings delimited by new lines.
* Each string (I assume) is guaranteed to have at least one number.
* Create a 2 digit number, the tens digit being the first number character in the string and the ones digit being the last number character in the string.
* Sum these 2 digit numbers from all lines.
* This total sum is the problem solution.


## Solve

1. Set `rsp` to proper value
2. Read input from stdin to address in `rsp`
3. Set `rsi` to `rsp` + `rax` (memory address at the end of the input)
4. Move `rsp` to `rdi`
5. Read byte from `rdi` into `edx`
6. If `edx` is equal to `ecx`
    1. zero `ecx`
    2. `JMP` Step 13
7. Sub `0x30` from `eax`
8. If `edx` > `0x09`
    1. `JMP` Step 15
9. Move `edx` into `eax`
10. If `ecx` is not `0`
    1. `JMP` Step 15
11. Set `ecx` to `0x0a`
12. `MUL` `eax` by `ecx`
13. `ADD` `eax` to `ebx`
14. `JMP` Step 5
15. `INC` `rdi`
16. If `rdi` is not equal to `rsi`
    1. `JMP` Step 5
17. Move `ebx` into `eax`
18. Move `0x0a` into `ecx`
19. Write `cl` to `rsi` (`\n`)
20. `DEC` `rsi`
21. Zero `rdx`
22. `DIV` `eax` by `ecx` (10)
23. `ADD` `0x30` to `edx`
24. Move `dl` to memory address in `rsi`
25. If `eax` is not `0`
    1. `JMP` Step 20
26. Write syscall

### Syscall - read - Read from stdin
`eax` = `0`
`edi` = `0`
`rsi` = `rsp`
`edx` = 0x7ffff000 (Notes on [man read](https://man7.org/linux/man-pages/man2/read.2.html))


### Steps 5 - 17
`rdi` - Current input memory pointer
`rsi` - Final input memory pointer
`eax` - Current digit in current line
`ebx` - Total sum
`ecx` - 0 at start of line, set to 10 after first digit read
`edx` - Holds byte of input


## Steps 18 - 26
`rax` - Total
`rdx` - Current digit in output
`rcx` - `0x10`
`rdi` - End of output string
`rsi` - Start of output string


### Syscall - write - Write answer to stdout
`eax` = `1`
`edi` = `1`
`rsi` = Output string memory address
`edx` = Length of output string (`rdi` - `rsi`)



## Notes

Step 4, We want to avoid doing memory dereferencing on `rsp` to avoid dealing with the SIB byte

Step 5, We read the byte inro `edx` because although it will be cached from then on in the CPU cache, we need to do multiple things with that byte, and registers after faster than L1 cache

Step 6, Compare to `ecx` instead of `0x0a` because we still skip adding 00 on lines with no numbers.

Step 12, We need to put `10` in a register to use the `MUL` instruction, and we can conveniently use this register as a flag for whether or not the first digit has been read from the string already.

Step 18, move `rbx` into `rax` because the `DIV` insturction only works with rax.

Step 22, remember that `DIV` uses `EDX:EAX`, so we need to clear `EDX` every time before we `DIV`


## Benchmarks

### 223,075 Instructions, ~190,000/avg cycles

```
$ perf stat --all-user -B -ddd ./bin < real-input 
[ANSWER REDACTED]

 Performance counter stats for './bin':

              0.15 msec task-clock:uhH                   #    0.280 CPUs utilized             
                 0      context-switches:uhH             #    0.000 /sec                      
                 0      cpu-migrations:uhH               #    0.000 /sec                      
                 1      page-faults:uhH                  #    6.556 K/sec                     
            190541      cpu_core/cycles/:uhH             #    1.249 GHz                       
     <not counted>      cpu_atom/cycles/:uhH                                                    (0.00%)
            223075      cpu_core/instructions/:uhH       #    1.17  insn per cycle            
     <not counted>      cpu_atom/instructions/:uhH                                              (0.00%)
             74680      cpu_core/branches/:uhH           #  489.615 M/sec                     
     <not counted>      cpu_atom/branches/:uhH                                                  (0.00%)
              4439      cpu_core/branch-misses/:uhH      #    5.94% of all branches           
     <not counted>      cpu_atom/branch-misses/:uhH                                             (0.00%)
             TopdownL1 (cpu_core)                 #      7.5 %  tma_backend_bound      
                                                  #     26.2 %  tma_bad_speculation    
                                                  #     53.0 %  tma_frontend_bound     
                                                  #     13.3 %  tma_retiring           
             23825      L1-dcache-loads:uhH              #  156.201 M/sec                     
     <not counted>      L1-dcache-loads:uhH                                                     (0.00%)
                57      L1-dcache-load-misses:uhH        #    0.24% of all L1-dcache accesses 
   <not supported>      L1-dcache-load-misses:uhH                                             
                 0      LLC-loads:uhH                    #    0.000 /sec                      
     <not counted>      LLC-loads:uhH                                                           (0.00%)
                 0      LLC-load-misses:uhH                                                   
     <not counted>      LLC-load-misses:uhH                                                     (0.00%)
   <not supported>      L1-icache-loads:uhH                                                   
     <not counted>      L1-icache-loads:uhH                                                     (0.00%)
     <not counted>      L1-icache-load-misses:uhH                                               (0.00%)
     <not counted>      L1-icache-load-misses:uhH                                               (0.00%)
     <not counted>      dTLB-loads:uhH                                                          (0.00%)
     <not counted>      dTLB-loads:uhH                                                          (0.00%)
     <not counted>      dTLB-load-misses:uhH                                                    (0.00%)
     <not counted>      dTLB-load-misses:uhH                                                    (0.00%)
   <not supported>      iTLB-loads:uhH                                                        
   <not supported>      iTLB-loads:uhH                                                        
     <not counted>      iTLB-load-misses:uhH                                                    (0.00%)
     <not counted>      iTLB-load-misses:uhH                                                    (0.00%)
   <not supported>      L1-dcache-prefetches:uhH                                              
   <not supported>      L1-dcache-prefetches:uhH                                              
   <not supported>      L1-dcache-prefetch-misses:uhH                                         
   <not supported>      L1-dcache-prefetch-misses:uhH                                         

       0.000544567 seconds time elapsed

       0.000000000 seconds user
       0.000666000 seconds sys
```

# Day 4

## Problem Breakdown

* Rows of numbers divided into 2 columns
* First column always has 10 numbers
* Second column always has 25 numbers
* Numbers are 2 digits with no leading 0s
* Points per line is 2^(Numbers in second column that match first column)
* Solution is sum of line totals


## Benchmarks

### 167,388 Instructions, ~110,000/avg cycles

```
$ perf stat --all-user -B -ddd ./bin < real-input 
[ANSWER REDACTED]

 Performance counter stats for './bin':

              0.07 msec task-clock:uhH                   #    0.372 CPUs utilized             
                 0      context-switches:uhH             #    0.000 /sec                      
                 0      cpu-migrations:uhH               #    0.000 /sec                      
                 1      page-faults:uhH                  #   14.486 K/sec                     
            108252      cpu_core/cycles/:uhH             #    1.568 GHz                       
     <not counted>      cpu_atom/cycles/:uhH                                                    (0.00%)
            167388      cpu_core/instructions/:uhH       #    1.55  insn per cycle            
     <not counted>      cpu_atom/instructions/:uhH                                              (0.00%)
              5027      cpu_core/branches/:uhH           #   72.821 M/sec                     
     <not counted>      cpu_atom/branches/:uhH                                                  (0.00%)
                17      cpu_core/branch-misses/:uhH      #    0.34% of all branches           
     <not counted>      cpu_atom/branch-misses/:uhH                                             (0.00%)
             TopdownL1 (cpu_core)                 #     65.5 %  tma_backend_bound      
                                                  #      0.4 %  tma_bad_speculation    
                                                  #      0.8 %  tma_frontend_bound     
                                                  #     33.3 %  tma_retiring           
             11582      L1-dcache-loads:uhH              #  167.777 M/sec                     
     <not counted>      L1-dcache-loads:uhH                                                     (0.00%)
                 9      L1-dcache-load-misses:uhH        #    0.08% of all L1-dcache accesses 
   <not supported>      L1-dcache-load-misses:uhH                                             
                 0      LLC-loads:uhH                    #    0.000 /sec                      
     <not counted>      LLC-loads:uhH                                                           (0.00%)
                 0      LLC-load-misses:uhH                                                   
     <not counted>      LLC-load-misses:uhH                                                     (0.00%)
   <not supported>      L1-icache-loads:uhH                                                   
     <not counted>      L1-icache-loads:uhH                                                     (0.00%)
     <not counted>      L1-icache-load-misses:uhH                                               (0.00%)
     <not counted>      L1-icache-load-misses:uhH                                               (0.00%)
     <not counted>      dTLB-loads:uhH                                                          (0.00%)
     <not counted>      dTLB-loads:uhH                                                          (0.00%)
     <not counted>      dTLB-load-misses:uhH                                                    (0.00%)
     <not counted>      dTLB-load-misses:uhH                                                    (0.00%)
   <not supported>      iTLB-loads:uhH                                                        
   <not supported>      iTLB-loads:uhH                                                        
     <not counted>      iTLB-load-misses:uhH                                                    (0.00%)
     <not counted>      iTLB-load-misses:uhH                                                    (0.00%)
   <not supported>      L1-dcache-prefetches:uhH                                              
   <not supported>      L1-dcache-prefetches:uhH                                              
   <not supported>      L1-dcache-prefetch-misses:uhH                                         
   <not supported>      L1-dcache-prefetch-misses:uhH                                         

       0.000185733 seconds time elapsed

       0.000242000 seconds user
       0.000000000 seconds sys
```

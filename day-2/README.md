# Day 2

## Problem Breakdown

* The input is a series of strings delimited by new lines.
* Each line line represents a game. If the game contains more than 12 red cubes, 13 green cubes, or 14 blue cubes, that game is invalid.
* Sum the game number for each valid gamei (max game number is 100)
* This sum is the answer


## Solve

I'm going to do a write up on this after AoC ends. This was incredibly complicated becuase I optimized a lot to reduce branching


## Benchmarks

### 26,770 instructions, ~30,000/avg cycles

```
$ perf stat --all-user -B -ddd ./bin < real-input 
[ANSWER REDACTED]

 Performance counter stats for './bin':

              0.05 msec task-clock:uhH                   #    0.217 CPUs utilized             
                 0      context-switches:uhH             #    0.000 /sec                      
                 0      cpu-migrations:uhH               #    0.000 /sec                      
                 1      page-faults:uhH                  #   22.032 K/sec                     
             29228      cpu_core/cycles/:uhH             #    0.644 GHz                       
     <not counted>      cpu_atom/cycles/:uhH                                                    (0.00%)
             26770      cpu_core/instructions/:uhH       #    0.92  insn per cycle            
     <not counted>      cpu_atom/instructions/:uhH                                              (0.00%)
              2769      cpu_core/branches/:uhH           #   61.007 M/sec                     
     <not counted>      cpu_atom/branches/:uhH                                                  (0.00%)
               225      cpu_core/branch-misses/:uhH      #    8.13% of all branches           
     <not counted>      cpu_atom/branch-misses/:uhH                                             (0.00%)
             TopdownL1 (cpu_core)                 #     57.8 %  tma_backend_bound      
                                                  #     18.6 %  tma_bad_speculation    
                                                  #      7.9 %  tma_frontend_bound     
                                                  #     15.6 %  tma_retiring           
              2370      L1-dcache-loads:uhH              #   52.216 M/sec                     
     <not counted>      L1-dcache-loads:uhH                                                     (0.00%)
                 2      L1-dcache-load-misses:uhH        #    0.08% of all L1-dcache accesses 
   <not supported>      L1-dcache-load-misses:uhH                                             
                 0      LLC-loads:uhH                    #    0.000 /sec                      
     <not counted>      LLC-loads:uhH                                                           (0.00%)
                 0      LLC-load-misses:uhH                                                   
     <not counted>      LLC-load-misses:uhH                                                     (0.00%)
   <not supported>      L1-icache-loads:uhH                                                   
     <not counted>      L1-icache-loads:uhH                                                     (0.00%)
     <not counted>      L1-icache-load-misses:uhH                                               (0.00%)
     <not counted>      L1-icache-load-misses:uhH                                               (0.00%)
     <not counted>      dTLB-loads:uhH                                                          (0.00%)
     <not counted>      dTLB-loads:uhH                                                          (0.00%)
     <not counted>      dTLB-load-misses:uhH                                                    (0.00%)
     <not counted>      dTLB-load-misses:uhH                                                    (0.00%)
   <not supported>      iTLB-loads:uhH                                                        
   <not supported>      iTLB-loads:uhH                                                        
     <not counted>      iTLB-load-misses:uhH                                                    (0.00%)
     <not counted>      iTLB-load-misses:uhH                                                    (0.00%)
   <not supported>      L1-dcache-prefetches:uhH                                              
   <not supported>      L1-dcache-prefetches:uhH                                              
   <not supported>      L1-dcache-prefetch-misses:uhH                                         
   <not supported>      L1-dcache-prefetch-misses:uhH                                         

       0.000209427 seconds time elapsed

       0.000318000 seconds user
       0.000000000 seconds sys
```
